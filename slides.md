---
title: Web Components
theme: moon
verticalSeparator: ===
---

# Web Components

The _What_, The _Why_, and the _How_

<br/>
<small>ver. 2022-10-13</small>

---

## Agenda

- What are Web Components<br/>
  <small>issues &nbsp; | &nbsp; vision & benefits &nbsp; | &nbsp; future &nbsp; | &nbsp; support  &nbsp; | &nbsp;  uses</small><br/><br/>
- Specifications<br/>
  <small>custom elements &nbsp; | &nbsp; shadow dom &nbsp; | &nbsp; html template &nbsp; | &nbsp; es module</small><br/><br/>
- Workflow<br/>
  <small>design system & ux mockups &nbsp; -> &nbsp; component dev &nbsp; -> &nbsp; library publishing -> &nbsp; usage</small>
  <br/><br/>
- Demo<br/>
  <small>usage & authoring perspectives</small><br/><br/>
- References, Next Steps

<br/>

===

### Typographical Conventions

- _highlighted 1 text_
  - for calling attention to some parts of the text
- **highlighted 2 text**
  - for emphasizing key concepts, ideas, or aspects
- [links text](...)
- `command`
  - for showing code or command line elements<br/>
- and blockquotes:
  > &nbsp; for quotes <small>and/or</small> paragraph highlighting

---

## What are Web Components

<small>issues &nbsp; | &nbsp; vision & benefits &nbsp; | &nbsp; future</small>

===

### Issues

- For some enough years, the **JavaScript world is a jungle**.<br/>
  <small>There are too many frameworks, various JS specs, different modules bundles.</small>
- **JavaScript fatigue** is a well known issue.
- It's **a fragmented world**, with little to **no reusability**.<br/>
  <small>Efficiency, delivery, and maintenance are directly affected.</small>

![](./img/wondering.png)<!-- .element width="30%" -->

===

### Vision & Benefits

- User interfaces build with the Web Components are **interoperable with any JavaScript framework**.<br/><br/>
- UI components built with Web Components have a **much greater lifespan** than a component built with a library since browser specs exist for a very long time.

===

### Funny history sidenote

- In the past:
  - Extending the prototype of a native HTML element was considered a bad practice.<br/><br/>
- Nowadays:
  - Through custom elements, we can extend from native HTML elements, retaining the accessibility behaviors of the element it is extended from.

![](./img/thinking_emoji.jpg)<!-- .element width="10%" -->

===

### Future 

- Google reported Web Components usage got almost _4 times_ higher since 2019.
  <small>Accoring to [these chrome stats](https://chromestatus.com/metrics/feature/popularity#CustomElementRegistryDefine), 19% of websites include a custom element.</small>

- Web Components is a set of Web standards, just like HTML and CSS.<br/><small>These won't dissapear or get deprecated over night. These are here to stay.</small>

- Large companies like Google, Microsoft, and Salesforce have adopted Web Components for their open source UI libraries.

===

### Browser Support

![](./img/browser_support.png)<!-- .element width="75%" -->

===

### Uses of Web Components

- Being so flexible and reusable, there are many use cases where Web Components are a good fit. These include:
  - **Static websites**
    - <small>Since vanilla JavaScript can be used, there is a lot of freedom.</small>
  - **Standalone widgets**
    - <small>Again, since even vanilla JavaScript can be used, WC custom elements can exist as <br/>standalone widgets in a page, fully isolated in their own space (own isolated DOM).</small>
  - **Single page applications**
    - <small>Web Components are simply complementing a SPA framework, providing the UI widgets.<br/>Features like routing, state mgmt, and data binding remain in the SPA's responsibility.</small>
  - **UI Library**
    - <small>Due to their interoperability and portability, UI libraries built with Web Components<br/>make a lot of sense, being compatible with any framework.</small>

===

### Challenges

- Some **Styling Difficulties** are addressed:
  - Scoped CSS using `:host` classes might be needed.
  - Custom properties cascade into Web Components.
  - Take advantage of shadow parts<br/><br/>
- **Server Side Rendering**
  - Techniques like app hydration can be used.
- **Z-Index**

---

## Specifications

<small>custom elements &nbsp; | &nbsp; shadow dom &nbsp; | &nbsp; html template &nbsp; | &nbsp; es module</small>

===

### Four Specs

Web Components standard consists on four topics, with their specifications:
1. **Custom Elements**<br/><br/>
2. **Shadow DOM**<br/><br/>
3. **HTML Template**<br/><br/>
4. **ES Module**

<br/><br/>

As a reference, [github.com/WICG/webcomponents](https://github.com/WICG/webcomponents) repository<br/>can be considered a starting point.

===

### Four Specs &nbsp; <small><small>(cont'd)</small></small>

Each of those four specifications can be used on their own. But combined, these:
- Allow developers to define their own tags (**custom element**)
- Whose styles are encapsulated and isolated (**shadow dom**)
- That can be restamped many times (**template**)
- And have a consistent way of being integrated into applications (**es module**)

===

### Custom Elements
<br/>

`TL;DR ` Custom elements allow web developers to define new HTML tags,<br/>extend existing ones, and create reusable web components.

<br/>

This specification describes the method for enabling the author<br/>to define and use new types of DOM elements in a document.

<br/>

- It is being incorporated into:
  - the [W3C DOM specification](https://www.w3.org/TR/dom41/#interface-element) and the [WHATWG DOM Standard](https://dom.spec.whatwg.org/#interface-element),
  - the [W3C HTML specification](https://www.w3.org/TR/html53/semantics-scripting.html#custom-elements-core-concepts) and the [WHATWG HTML Standard](https://html.spec.whatwg.org/multipage/custom-elements.html#custom-elements),
  - and other relevant specifications.

===

### Custom Elements &nbsp; <small><small>(cont'd)</small></small>

- This spec lays the foundation of designing and using new types of DOM elements.
- As expected from a reusable component, a CE **contains** both:
  - **Styling** &nbsp; (presentation)
  - Business **logic** &nbsp; (behavior)<br/><br/>
- There are three types:
  - Autonomous custom elements &nbsp; <small>fully custom HTML tags</small>
  - Customized built-in elements &nbsp; <small>extending existing HTML tags with custom functionality</small>
  - Form-associated custom elements &nbsp; <small>API for autonomous elements for form validation needs</small>

===

### Custom Elements &nbsp; <small><small>(cont'd)</small></small>

- Like `<p>` and `<img>` HTML elements, Web Components are part of the HTML spec.<br/><br/>
- A customized built-in element that extends from `HTMLButtonElement` includes all the traits of a typical HTML `<button>` element.
  - When the user presses Enter or Space, the button is activated without any custom logic (for things such as accessibility).

===

### Custom Elements - Short example

Here is a simple definition of a custom (autonomous) element:
```js
<script>

        // To create a custom element, we use ES6 class syntax.
        class HelloWorld extends HTMLElement {

            constructor() {
                super()
                // Create a shadow dom.
                const shadow = this.attachShadow({ mode: 'open' });
                // Create an element.
                const heading = document.createElement('h2');
                heading.textContent = 'Hello World';
                // Append the element to the shadow dom.
                shadow.appendChild(heading);
            }

        }
        // `customElements` provides methods for registering, unregistering and querying custom elements.
        customElements.define("hello-world", HelloWorld);

</script>
```

===

### Custom Elements - Short example &nbsp; <small><small>(cont'd)</small></small>

- Now to use that simple `HelloWorld` custom element is even simpler:
```js
<body>
    <hello-world></hello-world>
</body>
```
- That's because no attributes (to pass) nor events (to catch) exist in this case.

===

### Custom Elements - Reactions

- **Reactions** to certain lifecycle events can be defined, the main ones being:<br/>
| Name | Called |
| --- | --- |
| `constructor()` | when an instance of the element is created or upgraded. |
| `connectedCallback()` | every time the element is inserted into the DOM. |
| `disconnectedCallback()` | every time the element is removed from the DOM. |
| `attributeChangedCallback`<br/>`(attrName, oldVal, newVal)` | an observed attribute has been added, removed, updated, or replaced. |

- The observed attributes are declared using `observedAttributes()`:
  ```js
  static get observedAttributes() {  return ['attribute1', 'attribute2'];  }
  ```

===

### Custom Elements - Props, attrs, events

- Properties passed and used in JS reflect as attributes of the HTML element.<br/>
  ```js
  div.id = 'someId';     ---(reflects as)-->     <div id="someId">
  ```
  This keeps the element's DOM representation in sync with its JavaScript state.

- Since a custom element is no different than using a `<div>` or any other element, event listeners can be attached.

===

### Shadow DOM

- DOM is a browser technology (understood and specific to a Web browser).
  - It is the backbone of a Web page.
  - The standard DOM is called **Light DOM** and it's all we see in DevTools.<br/><br/>
- **Shadow DOM** is a browser technology that:
  - provides **encapsulation** for a HTML element,
  - **isolating** the HTML element’s template from the rest of DOM.<br/><br/>
- It is like another DOM that is included in the standard DOM.
  - In the tech jargon, it is  __attached__ to the `host` element<br/>
    (the top level element that employs Shadow DOM).

===

### Shadow DOM &nbsp; <small><small>(cont'd)</small></small>

- When developing UI components it’s extremely helpful to rely on an **isolated scope** for the component, so CSS and JavaScript collisions can’t happen.
- Shadow DOM provides a way for an element to own, render, and style a chunk of **DOM that is separate** from the rest of the page.
  - You could even hide away an entire app within a single tag!<br/><br/>
- There is also Declarative Shadow DOM spec that:
  - allows you declare Shadow DOM directly in HTML, without the need for JavaScript.
  - opens the doors for server-side rendering Shadow DOM and reduces the complexity when testing Web Components.

===

### HTML Template

- The `<template>` tag, first introduced in HTML5, allows HTML to be predefined and reused throughout DOM.<br/>
- It allows for custom element templates to be rendered performantly when HTML templates are used in conjunction with Shadow DOM.

===

### HTML Template &nbsp; <small><small>(cont'd)</small></small>

```js
    <hello-world>
        <h3>Hello from the slot.</h3>
        <h3 slot="namedSlot">Hello from the named slot.</h3>
    </hello-world>

    <template id="template_1">
        <slot></slot>
        <slot name="namedSlot"></slot>
    </template>

    <script>
        class HelloWorld extends HTMLElement {
            constructor() {
                ...
                // Create an element.
                const template = document.getElementById('template_1');
                const heading = template.content.cloneNode(true);
                // Append the element to the shadow dom.
                shadow.appendChild(heading);
            }
        }
```
<small>HTML templates are parsed but not rendered until cloned. This behavior allows a performant rendering<br/>when HTML templates are used in conjunction with Shadow DOM.</small>

===

### ES Module

- The [ES Module specification](https://html.spec.whatwg.org/multipage/webappapis.html#integration-with-the-javascript-module-system) addresses how JS modules can be bundled and imported in other JS projects.
  - This is an industry accepted standard, already used in the market.

---

## Workflow

<small>design system & ux mockups &nbsp; -> &nbsp; component dev &nbsp; -> &nbsp; library publishing -> &nbsp; usage</small>

===

### Design System with Web Components

- The UI components that are developed are part of a **design system**.
- A design system is a set of guidelines for the look and feel of a product or project.
  - It contains **tokens**, **components**, **layouts**, and potentially even  more.
- The journey:
  - From tokens as constants, reusable elements <small>(like color, size, spacing)</small>
  - And UX mockups in Figma
  - To development of components <small>(with distinct purpose, L&F)</small>
  - Exposing them in Storybook
  - To building and publishing a library <small>(ready to be used)</small>

===

![](./img/design_system_overview.png)<!-- .element width="90%" -->

---

## Demo

Showcasing both the usage and authoring perspectives.

`@ Florin Mihalcea`: The floor is yours, mate! :)

---

## References

===

### References

- [Prime Development Kit<small> &nbsp; &nbsp; on Confluence</small>](https://signicat.atlassian.net/wiki/spaces/CDX/pages/193410760817/Prime+Development+Kit)
- [Prime Components<small> &nbsp; &nbsp; on Figma</small>](https://www.figma.com/file/y12Aeg0eH0b0AiXg7OIFjv/Signicat-Prime---Master-Branch-(WIP))
- [Prime CSS<small> &nbsp; &nbsp; on Storybook</small>](https://signicat.gitlab.io/corporate-systems/ui-components/prime-css/)
- [MDN's web-components-examples<small> &nbsp; &nbsp; repo</small>](https://github.com/mdn/web-components-examples)
- [Vue.js and Web Components<small> &nbsp; &nbsp; showing how Vue can author and use WCs</small>](https://vuejs.org/guide/extras/web-components.html)
- [Custom Elements Everywhere<small> &nbsp; &nbsp; showing SPA frameworks readiness</small>](https://custom-elements-everywhere.com/)

---

## Next Steps

- An **MVP** to capture the primary needs on UI components<br/>
  and challenges during the process, and validate the concept.<br/><br/>
- **The Workflow** (from UX, to Development, and Publishing) needs further experimentation and prototyping.<br/><br/>
- A **collaboration model** must be defined and governed.
  - Any UI developer Signicat can contribute and benefit.<br/><br/>
- We (CDX tribe) will **keep you posted** with updates on this topic space.

---

<br/><br/>

## Thanks a minion for watching!

![](./img/thanks.png)<!-- .element height="50%" width="30%" -->
