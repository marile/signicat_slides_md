
Instructions
------------


1. Use font-face declaration Fonts.

@font-face {font-family: "MabryPro-Regular";
  src: url("mabry-pro-regular.eot"); /* IE9*/
  src: url("mabry-pro-regular.eot?#iefix") format("embedded-opentype"), /* IE6-IE8 */
  url("mabry-pro-regular.woff2") format("woff2"), /* chrome、firefox */
  url("mabry-pro-regular.woff") format("woff"), /* chrome、firefox */
  url("mabry-pro-regular.ttf") format("truetype"), /* chrome、firefox、opera、Safari, Android, iOS 4.2+*/
  url("mabry-pro-regular.svg#MabryPro-Regular") format("svg"); /* iOS 4.1- */
}



2. Settings font css style

.demo{
    font-family:"MabryPro-Regular" !important;
    font-size:16px;font-style:normal;
    -webkit-font-smoothing: antialiased;
    -webkit-text-stroke-width: 0.2px;
    -moz-osx-font-smoothing: grayscale;}


3. View DEMO

<div class="demo"> Check this font out! </div>




