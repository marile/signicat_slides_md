#!/bin/sh

## This script uses https://github.com/webpro/reveal-md tool
## for showing it locally in a browser.
## Use "npm i -g reveal-md" to install it globally.

reveal-md slides.md -w     \
	--css _/css/signicat.css               \
	--template template.html               \
	--assets-dir _
