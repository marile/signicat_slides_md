#!/bin/sh

PDF_OUTPUT=`pwd`

URL=http://localhost:1948/slides.md#/

docker run --rm -t --net=host -v ${PDF_OUTPUT}:/slides astefanutti/decktape -s 1024x800 reveal ${URL} slides.pdf
