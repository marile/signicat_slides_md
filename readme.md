# Signicat Slides in Markdown

This is a setup being used for those nerds that like markdown and less the standard tools.<br/>
Of course, it may come with some limitations (on certain layout desires or fancy animations).

For comments, ideas, or contribution, you can find its [repo here](https://gitlab.com/marile/signicat_slides_md). It's mainly based on [reveal.md](https://github.com/webpro/reveal-md) with some personal taste regarding cosmetics and Signicat's new branding elements.

Enjoy it!

<br/>

## Prequisites

Very few:
1. Node.js
2. Docker

<br/>

## Usage Notes

- Run `npm install` to install the dependencies.
  - This setup is based on `reveal-md`.
  - You may also use `npm i -g reveal-md` to install it globally.
- Run `./show.sh` to open the slides in a browser.
- Run `./print.sh` to generate the PDF format of the slides.
  - This needs the `./show.sh` to be running as well.

<br/>

## Navigation Shortcuts

Use the arrows keys to navigate through the pages:
- left and right through the main pages (separated by `---`)
- up and down through subpages (separated by `===`)

Use `Esc` to see the overview of all the pages and quickly jump through different topics.

Use `b` to toggle "screen off" effect.

Use `?` to see all the keyboard shortcuts that exist.

<br/>